﻿using Domain.Entities;
using Infrastructure.EntityFramework;
using Infrastructure.Repositories.Common;
using Infrastructure.Repositories.Contracts;

namespace Infrastructure.Repositories
{
    public class UserRepository : Repository<User, long>, IUserRepository
    {
        public UserRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
