﻿namespace WebApi.Contracts.Messages
{
    public class Move
    {
        public int Position { get; set; }

        public string User { get; set; }
    }
}