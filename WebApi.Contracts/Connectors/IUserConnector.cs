﻿using WebApi.Contracts.Models;

namespace WebApi.Contracts.Connectors
{
    public interface IUserConnector
    {
        Task<IEnumerable<UserModel>> GetAllAsync();
    }
}