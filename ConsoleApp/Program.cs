﻿using System;
using System.IO;
using AutoMapper;
using BusinessLogic.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ConsoleApp
{
	public class Program
	{
		private readonly IUserService _userService;
		private readonly ILogger<Program> _logger;
		private IMapper _mapper;
		
		static void Main(string[] args)
		{
			var host = CreateHostBuilder(args).Build();

			host.Services.GetRequiredService<Program>().Run();
		}

		public Program(
			IUserService userService,
			ILogger<Program> logger,
			IMapper mapper)
		{
			_userService = userService;
			_logger = logger;
			_mapper = mapper;
		}

		public void Run()
		{
			var users = _userService.GetAll().Result;

			foreach (var user in users)
			{
				Console.WriteLine($"User: Id = '{user.Id}', FirstName = '{user.FirstName}', LastName = '{user.LastName}', Email = '{user.Email}', Password = '{user.Password}'");
			}
		}

		private static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureServices(services =>
				{
					var builder = new ConfigurationBuilder()
						.SetBasePath(Directory.GetCurrentDirectory())
						.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);

					IConfiguration configuration = builder.Build();

					new Startup(configuration).ConfigureServices(services);
				});
	}
}
