﻿using Domain.Entities;
using Infrastructure.Repositories.Common.Contracts;

namespace Infrastructure.Repositories.Contracts
{
    public interface IUserRepository : IRepository<User, long>
    {
    }
}