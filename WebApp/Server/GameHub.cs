﻿using Microsoft.AspNetCore.SignalR;

namespace WebApp.Server
{
    public class GameHub : Hub<IGameHub>
    {
        public async Task JoinGame(Guid gameId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, gameId.ToString());
        }
    }
}