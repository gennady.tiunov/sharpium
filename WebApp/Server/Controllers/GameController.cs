﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using WebApi.Contracts.Messages;

namespace WebApp.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private readonly IHubContext<GameHub, IGameHub> _gameHubContext;

        public GameController(IHubContext<GameHub, IGameHub> gameHubContext)
        {
            _gameHubContext = gameHubContext;
        }

        [HttpPost("makemove/{gameId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> MakeMove(Guid gameId)
        {
            var rnd = new Random();

            var move = new Move { Position = rnd.Next(), User = "user" };

            //await _gameHubContext.Clients.All.MoveMade(move);
            await _gameHubContext.Clients.Group(gameId.ToString()).MoveMade(move);

            return Ok();
        }
    }
}