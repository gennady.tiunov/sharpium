﻿using WebApi.Contracts.Messages;

namespace WebApp.Server
{
    public interface IGameHub
    {
        Task MoveMade(Move move);
    }
}
