﻿namespace WebApp.Shared
{
    public struct BoardCoordinates
    {
        public BoardCoordinates(
            ushort row,
            ushort column)
        {
            Row = row;
            Column = column;
        }

        public ushort Row { get; private set; }

        public ushort Column { get; private set; }
    }
}