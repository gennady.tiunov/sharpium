﻿namespace WebApp.Shared
{
    public enum GameStatus
    {
        XWon,
        OWon,
        Draw,
        GameInProgress
    }
}