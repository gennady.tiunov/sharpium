﻿namespace WebApp.Shared
{
    public static class GameHelper
    {
        public static GameStatus CalculateGameStatus(char[] cells)
        {
            var winningCombos = new[,]
            {
                    { 0, 1, 2 },
                    { 3, 4, 5 },
                    { 6, 7, 8 },
                    { 0, 3, 6 },
                    { 1, 4, 7 },
                    { 2, 5, 8 },
                    { 0, 4, 8 },
                    { 2, 4, 6 },
            };
            for (int i = 0; i < 8; i++)
            {
                var value1 = cells[winningCombos[i, 0]];
                var value2 = cells[winningCombos[i, 1]];
                var value3 = cells[winningCombos[i, 2]];


                if (value1 != ' ' && value1 == value2 && value1 == value3)
                {
                    return cells[winningCombos[i, 0]] == 'X' ? GameStatus.XWon : GameStatus.OWon;
                }
            }

            bool isBoardFull = true;
            for (int i = 0; i < cells.Length; i++)
            {
                if (cells[i] == ' ')
                {
                    isBoardFull = false;
                    break;
                }
            }
            return isBoardFull ? GameStatus.Draw : GameStatus.GameInProgress;
        }
    }
}
