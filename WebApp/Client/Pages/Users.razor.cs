﻿using Microsoft.AspNetCore.Components;
using WebApi.Contracts.Connectors;
using WebApi.Contracts.Models;

namespace WebApp.Client.Pages
{
    public partial class Users
    {
        [Inject]
        public IUserConnector UserConnector { get; set; }

        public IEnumerable<UserModel> UserList { get; private set; }

        protected async override Task OnInitializedAsync()
        {
            UserList = await UserConnector.GetAllAsync();
        }
    }
}