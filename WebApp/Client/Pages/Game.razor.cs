﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;
using WebApi.Contracts.Messages;
using WebApp.Client.WebApiConnectors;

namespace WebApp.Client.Pages
{
    public partial class Game
    {
        [Inject]
        public HubConnection HubConnection { get; set; }

        [Inject]
        public IGameConnector GameConnector { get; set; }

        public string LatestMove { get; set; }

        protected async override Task OnInitializedAsync()
        {
            HubConnection.On<Move>("MoveMade", move =>
            {
                LatestMove = $"Position: {move.Position}";

                StateHasChanged();
            });
        }
        private async Task StartGame(Guid gameId)
        {
            await HubConnection.InvokeAsync("JoinGame", gameId);
        }

        private async Task MakeMove(Guid gameId)
        {
            await GameConnector.MakeMoveAsync(gameId);
        }
    }
}