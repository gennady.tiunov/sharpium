using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.AspNetCore.SignalR.Client;
using WebApi.Contracts.Connectors;
using WebApp.Client;
using WebApp.Client.WebApiConnectors;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddHttpClient<IUserConnector, UserConnector>(
    client => client.BaseAddress = new Uri("https://localhost:44306/"));

builder.Services.AddHttpClient<IGameConnector, GameConnector>(
    client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress));

builder.Services.AddSingleton<HubConnection>(sp =>
{
    var navigationManager = sp.GetRequiredService<NavigationManager>();

    return new HubConnectionBuilder()
      .WithUrl(navigationManager.ToAbsoluteUri("/gamehub"))
      .WithAutomaticReconnect()
      .Build();
});

await builder.Build().RunAsync();
