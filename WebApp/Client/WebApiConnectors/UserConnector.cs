﻿using System.Text.Json;
using WebApi.Contracts.Connectors;
using WebApi.Contracts.Models;

namespace WebApp.Client.WebApiConnectors
{
    public class UserConnector : IUserConnector
    {
        private readonly HttpClient _httpClient;

        public UserConnector(
            HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<UserModel>> GetAllAsync()
        {
            return await JsonSerializer.DeserializeAsync<IEnumerable<UserModel>>
                (await _httpClient.GetStreamAsync("users"), new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
        }
    }
}
