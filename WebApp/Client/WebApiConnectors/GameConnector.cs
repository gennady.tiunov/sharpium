﻿namespace WebApp.Client.WebApiConnectors
{
    public class GameConnector : IGameConnector
    {
        private readonly HttpClient _httpClient;

        public GameConnector(
            HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task MakeMoveAsync(Guid gameId)
        {
            await _httpClient.PostAsync($"game/makemove/{gameId}", null);
        }
    }
}
