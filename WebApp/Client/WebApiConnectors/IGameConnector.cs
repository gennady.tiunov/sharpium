﻿namespace WebApp.Client.WebApiConnectors
{
    public interface IGameConnector
    {
        Task MakeMoveAsync(Guid gameId);
    }
}