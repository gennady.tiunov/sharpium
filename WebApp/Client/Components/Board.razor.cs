﻿using WebApp.Shared;

namespace WebApp.Client.Components
{
    public partial class Board
    {
		private const ushort RowCount = 3;
		private const ushort ColumnCount = 3;

		private readonly ushort CellCount = RowCount * ColumnCount;

        private bool _isXNext = true;

		private char[] Values;
		private string Status { get; set; }

		private string ClickedRow { get; set; }

		private string ClickedColumn { get; set; }

		public Board()
		{
			InitBordState();
		}

		private void PlayAgain()
		{
			InitBordState();
		}

		private void InitBordState()
		{
			Values = new[]
			{
				' ', ' ', ' ',
				' ', ' ', ' ',
				' ', ' ', ' '
			};

			_isXNext = true;
			Status = BuildStatusLabel(GameHelper.CalculateGameStatus(Values), _isXNext);

			ClickedRow = string.Empty;
			ClickedColumn = string.Empty;
		}

        private void HandleCellClick(ushort cellIndex)
		{
			var coordinates = IndexToCoordinates(cellIndex);

			ClickedRow = coordinates.Row.ToString();
			ClickedColumn = coordinates.Column.ToString();

			if (Values[cellIndex] != ' ')
			{
				return;
			}

			if (GameHelper.CalculateGameStatus(Values) != GameStatus.GameInProgress)
			{
				return;
			}

			Values[cellIndex] = _isXNext ? 'X' : 'O';
			_isXNext = !_isXNext;

			Status = BuildStatusLabel(GameHelper.CalculateGameStatus(Values), _isXNext);
		}

		private string BuildStatusLabel(
			GameStatus status,
			bool isXNext)
        {
			string statusLabel;

			switch (status)
			{
				case GameStatus.XWon:
					statusLabel = "Game finished - 'X' is the winner";
					break;

				case GameStatus.OWon:
					statusLabel = "Game finished - 'O' is the winner";
					break;

				case GameStatus.Draw:
					statusLabel = "Game finished - draw";
					break;

				default:
					char nextPlayer = isXNext ? 'X' : 'O';
					statusLabel = $"Next player: {nextPlayer}";
					break;
			}

			return statusLabel;
		}

		public BoardCoordinates IndexToCoordinates(
			ushort index)
		{
			var row = (ushort) (index / RowCount);
			var column = (ushort) (index % ColumnCount);

			return new BoardCoordinates(row, column);
		}
	}
}
