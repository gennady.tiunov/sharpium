using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLogic.Contracts
{
    public interface IUserService
    {
        Task<ICollection<UserDto>> GetAll();

        Task<UserDto> GetById(long id);

        Task<long> Create(UserDto userDto);

        Task Update(long id, UserDto userDto);

        Task Delete(long id);
    }
}