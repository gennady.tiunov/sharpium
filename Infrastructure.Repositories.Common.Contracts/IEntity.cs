namespace Infrastructure.Repositories.Common.Contracts
{
    public interface IEntity<TId>
    {
        TId Id { get; set; }
    }
}