using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Repositories.Common.Contracts
{
    public interface IRepository
    {
    }

    public interface IRepository<T, TPrimaryKey> : IReadRepository<T, TPrimaryKey>
        where T : IEntity<TPrimaryKey>
    {
        T Add(T entity);

        Task<T> AddAsync(T entity);

        void AddRange(List<T> entities);

        Task AddRangeAsync(ICollection<T> entities);

        void Update(T entity);

        bool Delete(TPrimaryKey id);

        bool Delete(T entity);

        bool DeleteRange(ICollection<T> entities);

        void SaveChanges();

        Task SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}