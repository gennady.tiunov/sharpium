﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Contracts;
using Domain.Entities;
using Infrastructure.Repositories.Contracts;

namespace BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public UserService(
            IMapper mapper,
            IUserRepository userRepository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task<ICollection<UserDto>> GetAll()
        {
            ICollection<User> entities = await _userRepository.GetAllAsync();
            return _mapper.Map<ICollection<User>, ICollection<UserDto>>(entities);
        }

        public async Task<UserDto> GetById(long id)
        {
            var user = await _userRepository.GetAsync(id);
            return _mapper.Map<UserDto>(user);
        }

        public async Task<long> Create(UserDto userDto)
        {
            var entity = _mapper.Map<UserDto, User>(userDto);
            var result = await _userRepository.AddAsync(entity);
            await _userRepository.SaveChangesAsync();
            return result.Id;
        }

        public async Task Update(long id, UserDto lessonDto)
        {
            var entity = _mapper.Map<UserDto, User>(lessonDto);
            entity.Id = id;
            _userRepository.Update(entity);
            await _userRepository.SaveChangesAsync();
        }

        public async Task Delete(long id)
        {
            var user = await _userRepository.GetAsync(id);
            user.Deleted = true; 
            await _userRepository.SaveChangesAsync();
        }
    }
}