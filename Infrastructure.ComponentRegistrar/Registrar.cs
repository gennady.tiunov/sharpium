﻿using BusinessLogic.Contracts;
using BusinessLogic.Services;
using Infrastructure.ComponentRegistrar.Settings;
using Infrastructure.EntityFramework;
using Infrastructure.Repositories;
using Infrastructure.Repositories.Contracts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.ComponentRegistrar
{
    public static class Registrar
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();

            services.AddSingleton(applicationSettings);

            return services
                .AddSingleton((IConfigurationRoot)configuration)
                .RegisterBusinessServices()
                .ConfigureContext(applicationSettings.ConnectionString)
                .RegisterRepositories();
        }

        private static IServiceCollection RegisterBusinessServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUserService, UserService>();

            return serviceCollection;
        }

        private static IServiceCollection RegisterRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUserRepository, UserRepository>();

            return serviceCollection;
        }
    }
}