CREATE DATABASE "TicTacToe"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

\connect "TicTacToe"

CREATE TABLE public."Users"
(
    "Id" bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "FirstName" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "LastName" character varying(50) COLLATE pg_catalog."default" NOT NULL,
    "Email" character varying(100) COLLATE pg_catalog."default" NOT NULL,
    "Password" character varying(50) COLLATE pg_catalog."default" NOT NULL,
	"Deleted" boolean NOT NULL,
    CONSTRAINT "Users_pkey" PRIMARY KEY ("Id")
)

TABLESPACE pg_default;

ALTER TABLE public."Users"
    OWNER to postgres;

INSERT INTO public."Users" ("FirstName", "LastName", "Email", "Password", "Deleted")
	VALUES ('Konstantin', 'Konstantinov', 'Konstantin.Konstantinov@gmail.com', '1qaz@WSX', 'FALSE');

INSERT INTO public."Users" ("FirstName", "LastName", "Email", "Password", "Deleted")
	VALUES ('Oleg', 'Olegov', 'Oleg.Olegov@gmail.com', '1qaz@WSX', 'FALSE');

INSERT INTO public."Users" ("FirstName", "LastName", "Email", "Password", "Deleted")
	VALUES ('Ivan', 'Ivanov', 'Ivan.Ivanov@gmail.com', '1qaz@WSX', 'FALSE')